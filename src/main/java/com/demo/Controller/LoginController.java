package com.demo.Controller;

import com.demo.Model.AccountKit;
import org.apache.tomcat.util.net.openssl.ciphers.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Controller
public class LoginController {

    private RestTemplate restTemplate = new RestTemplate();

    @GetMapping("/login")
    public String getLogin(){
        return "login";
    }

    @PostMapping("/login/submit")
    public String getIndex(AccountKit accountKit){
        System.out.println(accountKit);


        Map<String, Object> object=  restTemplate.getForObject("https://graph.accountkit.com/v1.3/access_token?grant_type=authorization_code&code="+ accountKit.getCode()+"&access_token=AA|2391683511114891|6374aa423da43adca9d15a64f6d28347",Map.class);
        System.out.println(object);

        String accessToken= (String) object.get("access_token");

        Map<String, Object> data= restTemplate.getForObject("https://graph.accountkit.com/v1.3/me/?access_token="+accessToken,Map.class);
        System.out.println(data);

        return "index";
    }

}
