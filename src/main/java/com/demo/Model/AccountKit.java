package com.demo.Model;

public class AccountKit {
        String code;
        String csrf;

        public AccountKit(String code, String csrf) {
            this.code = code;
            this.csrf = csrf;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getCsrf() {
            return csrf;
        }

        public void setCsrf(String csrf) {
            this.csrf = csrf;
        }

        @Override
        public String toString() {
            return "AccKitRequest{" +
                    "code='" + code + '\'' +
                    ", csrf='" + csrf + '\'' +
                    '}';
        }

}
